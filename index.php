<?php
/**
 * Forwarder/Router to doku.php
 *到doku.php的转发器/路由器
 *
 * In normal usage, this script simply redirects to doku.php. However it can also be used as a routing
 *在正常使用情况下，此脚本仅重定向到doku.php。 但是，它也可以用作路由
 * script with PHP's builtin webserver. It takes care of .htaccess compatible rewriting, directory/file
 *带有PHP内置网络服务器的脚本。 它负责.htaccess兼容的重写，目录/文件
 * access permission checking and passing on static files.
 *访问权限检查并传递静态文件。
 *
 * Usage example:
 *用法示例：
 *
 *   php -S localhost:8000 index.php
 * php -S本地主机：8000 index.php
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author     Andreas Gohr <andi@splitbrain.org>
 */
if(php_sapi_name() != 'cli-server') {
    header("Location: doku.php");
    exit;
}

# ROUTER starts below ＃读取每一个数从下面开始

# avoid path traversal #避免路径遍历
$_SERVER['SCRIPT_NAME'] = str_replace('/../', '/', $_SERVER['SCRIPT_NAME']);

# routing aka. rewriting ＃路由又名。 改写
if(preg_match('/^\/_media\/(.*)/', $_SERVER['SCRIPT_NAME'], $m)) {
    # media dispatcher ＃媒体调度员
    $_GET['media'] = $m[1];
    require $_SERVER['DOCUMENT_ROOT'] . '/lib/exe/fetch.php';

} else if(preg_match('/^\/_detail\/(.*)/', $_SERVER['SCRIPT_NAME'], $m)) {
    # image detail view ＃图像细节视图
    $_GET['media'] = $m[1];
    require $_SERVER['DOCUMENT_ROOT'] . '/lib/exe/detail.php';

} else if(preg_match('/^\/_media\/(.*)/', $_SERVER['SCRIPT_NAME'], $m)) {
    # exports ＃出口
    $_GET['do'] = 'export_' . $m[1];
    $_GET['id'] = $m[2];
    require $_SERVER['DOCUMENT_ROOT'] . '/doku.php';

} elseif($_SERVER['SCRIPT_NAME'] == '/index.php') {
    # 404s are automatically mapped to index.php ＃404自动映射到index.php
    if(isset($_SERVER['PATH_INFO'])) {
        $_GET['id'] = $_SERVER['PATH_INFO'];
    }
    require $_SERVER['DOCUMENT_ROOT'] . '/doku.php';

} else if(file_exists($_SERVER['DOCUMENT_ROOT'] . $_SERVER['SCRIPT_NAME'])) {
    # existing files ＃个现有文件

    # access limitiations ＃访问限制
    if(preg_match('/\/([\._]ht|README$|VERSION$|COPYING$)/', $_SERVER['SCRIPT_NAME']) or
        preg_match('/^\/(data|conf|bin|inc)\//', $_SERVER['SCRIPT_NAME'])
    ) {
        die('Access denied');
    }

    if(substr($_SERVER['SCRIPT_NAME'], -4) == '.php') {
        # php scripts ＃php脚本
        require $_SERVER['DOCUMENT_ROOT'] . $_SERVER['SCRIPT_NAME'];
    } else {
        # static files ＃个静态文件
        return false;
    }
}
# 404
