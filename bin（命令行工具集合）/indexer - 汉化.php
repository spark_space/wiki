＃！/ usr / bin / php
<？php

使用splitbrain \ phpcli \ CLI;
使用splitbrain \ phpcli \ Options;

if（！defined（'DOKU_INC'））define（'DOKU_INC'，realpath（dirname（__ FILE__）。'/../'）。'/'）;
define（'NOSESSION'，1）;
require_once（DOKU_INC。'inc / init.php'）;

/ **
 *从命令行更新搜索索引
 * /
class IndexerCLI扩展了CLI {

    私人$ quiet = false;
    私人$ clear =假;

    / **
     *在给定的$ options对象上注册选项和参数
     *
     * @param选项$ options
     * @返回无效
     * /
    受保护的功能设置（选项$ options）{
        $ options-> setHelp（
            '通过索引所有新页面或更改页面来更新searchindex。当-c选项为'时。
            “给定索引先被清除。”
        ）;

        $ options-> registerOption（
            '明确'，
            “在更新之前先清除索引”，
            'C'
        ）;
        $ options-> registerOption（
            '安静'，
            “不产生任何输出”，
            'q'
        ）;
    }

    / **
     *您的主程序
     *
     *参数和选项已在运行时解析
     *
     * @param选项$ options
     * @返回无效
     * /
    受保护的函数main（Options $ options）{
        $ this-> clear = $ options-> getOpt（'clear'）;
        $ this-> quiet = $ options-> getOpt（'quiet'）;

        if（$ this-> clear）$ this-> clearindex（）;

        $ this-> update（）;
    }

    / **
     *更新索引
     * /
    函数update（）{
        全球$ conf;
        $ data = array（）;
        $ this-> quietecho（“正在搜索页面...”）;
        搜索（$ data，$ conf ['datadir']，'search_allpages'，array（'skipacl'=> true））;
        $ this-> quietecho（count（$ data）。“找到的页面。\ n”）;

        foreach（$ data as $ val）{
            $ this-> index（$ val ['id']）;
        }
    }

    / **
     *索引给定页面
     *
     * @参数字符串$ id
     * /
    函数索引（$ id）{
        $ this-> quietecho（“ $ id ...”）;
        idx_addPage（$ id，！$ this-> quiet，$ this-> clear）;
        $ this-> quietecho（“ done。\ n”）;
    }

    / **
     *清除所有索引文件
     * /
    函数clearindex（）{
        $ this-> quietecho（“结算指数...”）;
        idx_get_indexer（）-> clear（）;
        $ this-> quietecho（“ done。\ n”）;
    }

    / **
     *打印消息（如果没有限制）
     *
     * @参数字符串$ msg
     * /
    函数quietecho（$ msg）{
        if（！$ this-> quiet）echo $ msg;
    }
}

//主
$ cli =新的IndexerCLI（）;
$ cli-> run（）;