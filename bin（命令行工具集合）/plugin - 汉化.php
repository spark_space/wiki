＃！/ usr / bin / php
<？php

使用splitbrain \ phpcli \ CLI;
使用splitbrain \ phpcli \ Colors;
使用splitbrain \ phpcli \ Options;

if（！defined（'DOKU_INC'））define（'DOKU_INC'，realpath（dirname（__ FILE__）。'/../'）。'/'）;
define（'NOSESSION'，1）;
require_once（DOKU_INC。'inc / init.php'）;

类PluginCLI扩展了CLI {

    / **
     *在给定的$ options对象上注册选项和参数
     *
     * @param选项$ options
     * @返回无效
     * /
    受保护的功能设置（选项$ options）{
        $ options-> setHelp（'Excecutes插件命令行工具'）;
        $ options-> registerArgument（'plugin'，'您要运行的插件CLI。不显示列表'，false）;
    }

    / **
     *您的主程序
     *
     *参数和选项已在运行时解析
     *
     * @param选项$ options
     * @返回无效
     * /
    受保护的函数main（Options $ options）{
        全局$ argv;
        $ argv = $ options-> getArgs（）;

        if（$ argv）{
            $ plugin = $ this-> loadPlugin（$ argv [0]）;
            if（$ plugin！== null）{
                $ plugin-> run（）;
            }其他{
                $ this-> fatal（'Command {cmd} not found。'，['cmd'=> $ argv [0]]）;
            }
        }其他{
            echo $ options-> help（）;
            $ this-> listPlugins（）;
        }
    }

    / **
     *列出可用的插件
     * /
    受保护的函数listPlugins（）{
        / ** @var Doku_Plugin_Controller $ plugin_controller * /
        全局$ plugin_controller;

        回显“ \ n”；
        回显“ \ n”；
        echo $ this-> colors-> wrap（'AVAILABLE PLUGINS：'，Colors :: C_BROWN）;复制代码
        回显“ \ n”；

        $ list = $ plugin_controller-> getList（'cli'）;
        sort（$ list）;
        if（！count（$ list））{
            echo $ this-> colors-> wrap（“没有提供CLI组件的插件可用\ n”，Colors :: C_RED）;
        }其他{
            $ tf = new \ splitbrain \ phpcli \ TableFormatter（$ this-> colors）;

            foreach（$ list as $ name）{
                $ plugin = $ this-> loadPlugin（$ name）;
                if（$ plugin === null）继续;
                $ info = $ plugin-> getInfo（）;

                回声$ tf-> format（
                    [2，'30％'，'*']，
                    [''，$ name，$ info ['desc']]，
                    [''，颜色:: C_CYAN，'']

                ）;
            }
        }
    }

    / **
     *实例化CLI插件
     *
     * @参数字符串$ name
     * @return DokuWiki_CLI_Plugin | null
     * /
    受保护的
    函数loadPlugin（$ name）{
        //执行插件CLI
        $ class =“ cli_plugin_ $ name”;
        if（class_exists（$ class））{
            返回新的$ class（）;
        }
        返回null;
    }
}

//主
$ cli =新的PluginCLI（）;
$ cli-> run（）;