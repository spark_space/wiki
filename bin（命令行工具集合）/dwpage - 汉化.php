＃！/ usr / bin / php
<？php

使用splitbrain \ phpcli \ CLI;
使用splitbrain \ phpcli \ Options;

if（！defined（'DOKU_INC'））define（'DOKU_INC'，realpath（dirname（__ FILE__）。'/../'）。'/'）;
define（'NOSESSION'，1）;
require_once（DOKU_INC。'inc / init.php'）;

/ **
 *从命令行检出并提交页面，同时保持历史记录
 * /
PageCLI类扩展CLI {

    protected $ force = false;
    保护$ username ='';

    / **
     *在给定的$ options对象上注册选项和参数
     *
     * @param选项$ options
     * @返回无效
     * /
    受保护的功能设置（选项$ options）{
        / *全局* /
        $ options-> registerOption（
            '力'，
            “强制为页面获取锁（通常是个好主意）”，
            'F'
        ）;
        $ options-> registerOption（
            '用户'，
            以该用户身份工作。默认为当前的CLI用户”，
            'u'，
            '用户名'
        ）;
        $ options-> setHelp（
            “实用程序帮助命令行Dokuwiki页面编辑，允许”。
            “要签出以进行编辑然后在更改后提交的页面”
        ）;

        / *结帐命令* /
        $ options-> registerCommand（
            '查看'，
            '使用Wiki ID从存储库中检出文件并获取'。
            锁定页面。'。“ \ n”。
            '如果指定了work_file，则将页面复制到其中。'。
            否则默认为与当前Wiki页面相同。
            “工作目录”。
        ）;
        $ options-> registerArgument（
            “ wikipage”，
            “要签出的Wiki页面”，
            真正，
            '查看'
        ）;
        $ options-> registerArgument（
            '工作文件'，
            “如何命名本地结帐”，
            假，
            '查看'
        ）;

        / *提交命令* /
        $ options-> registerCommand（
            '承诺'，
            '使用指定的值将working_file检入到存储库中。
            “ Wiki ID，存档先前版本。”
        ）;
        $ options-> registerArgument（
            '工作文件'，
            “要提交的本地文件”，
            真正，
            '承诺'
        ）;
        $ options-> registerArgument（
            “ wikipage”，
            “要创建或更新的Wiki页面”，
            真正，
            '承诺'
        ）;
        $ options-> registerOption（
            '信息'，
            “描述更改的摘要（必填）”，
            'm'，
            '摘要'，
            '承诺'
        ）;
        $ options-> registerOption（
            '不重要的'，
            '微小的变化'，
            't'，
            假，
            '承诺'
        ）;

        / *锁定命令* /
        $ options-> registerCommand（
            '锁'，
            “获取或更新Wiki页面的锁”
        ）;
        $ options-> registerArgument（
            “ wikipage”，
            “要锁定的维基页面”，
            真正，
            '锁'
        ）;

        / *解锁命令* /
        $ options-> registerCommand（
            '开锁'，
            “删除维基页面的锁。”
        ）;
        $ options-> registerArgument（
            “ wikipage”，
            “要解锁的维基页面”，
            真正，
            '开锁'
        ）;
    }

    / **
     *您的主程序
     *
     *参数和选项已在运行时解析
     *
     * @param选项$ options
     * @返回无效
     * /
    受保护的函数main（Options $ options）{
        $ this-> force = $ options-> getOpt（'force'，false）;
        $ this-> username = $ options-> getOpt（'user'，$ this-> getUser（））;

        $ command = $ options-> getCmd（）;
        $ args = $ options-> getArgs（）;
        开关（$命令）{
            案例“结帐”：
                $ wiki_id = array_shift（$ args）;
                $ localfile = array_shift（$ args）;
                $ this-> commandCheckout（$ wiki_id，$ localfile）;
                打破;
            案例“提交”：
                $ localfile = array_shift（$ args）;
                $ wiki_id = array_shift（$ args）;
                $ this-> commandCommit（
                    $ localfile，
                    $ wiki_id，
                    $ options-> getOpt（'message'，''），
                    $ options-> getOpt（'平凡'，假）
                ）;
                打破;
            情况“锁定”：
                $ wiki_id = array_shift（$ args）;
                $ this-> obtainLock（$ wiki_id）;
                $ this-> success（“ $ wiki_id已锁定”）;
                打破;
            案例“解锁”：
                $ wiki_id = array_shift（$ args）;
                $ this-> clearLock（$ wiki_id）;
                $ this-> success（“ $ wiki_id已解锁”）;
                打破;
            默认：
                echo $ options-> help（）;
        }
    }

    / **
     *检出文件
     *
     * @param字符串$ wiki_id
     * @参数字符串$ localfile
     * /
    受保护的函数commandCheckout（$ wiki_id，$ localfile）{
        全球$ conf;

        $ wiki_id = cleanID（$ wiki_id）;
        $ wiki_fn = wikiFN（$ wiki_id）;

        if（！file_exists（$ wiki_fn））{
            $ this-> fatal（“ $ wiki_id尚不存在”）;
        }

        if（empty（$ localfile））{
            $ localfile = getcwd（）。'/' utf8_basename（$ wiki_fn）;
        }

        if（！file_exists（dirname（$ localfile）））{
            $ this-> fatal（“ Directory”。dirname（$ localfile）。“不存在”）;
        }

        if（stristr（realpath（dirname（$ localfile）），realpath（$ conf ['datadir']）））！== false）{
            $ this-> fatal（“尝试将文件检出到数据目录中-不允许”）；
        }

        $ this-> obtainLock（$ wiki_id）;

        if（！copy（$ wiki_fn，$ localfile））{
            $ this-> clearLock（$ wiki_id）;
            $ this-> fatal（“无法将$ wiki_fn复制到$ localfile”）；
        }

        $ this-> success（“ $ wiki_id> $ localfile”）;
    }

    / **
     *将文件另存为新页面修订
     *
     * @参数字符串$ localfile
     * @param字符串$ wiki_id
     * @参数字符串$ message
     * @参数布尔$ minor
     * /
    受保护的函数commandCommit（$ localfile，$ wiki_id，$ message，$ minor）{
        $ wiki_id = cleanID（$ wiki_id）;
        $ message = trim（$ message）;

        if（！file_exists（$ localfile））{
            $ this-> fatal（“ $ localfile不存在”）;
        }

        if（！is_可读（$ localfile））{
            $ this-> fatal（“无法从$ localfile中读取”）;
        }

        if（！$ message）{
            $ this-> fatal（“需要摘要消息”）;
        }

        $ this-> obtainLock（$ wiki_id）;

        saveWikiText（$ wiki_id，file_get_contents（$ localfile），$ message，$ minor）;

        $ this-> clearLock（$ wiki_id）;

        $ this-> success（“ $ localfile> $ wiki_id”）;
    }

    / **
     *锁定给定页面或退出
     *
     * @param字符串$ wiki_id
     * /
    受保护的函数gainLock（$ wiki_id）{
        if（$ this-> force）$ this-> deleteLock（$ wiki_id）;

        $ _SERVER ['REMOTE_USER'] = $ this->用户名；

        if（checklock（$ wiki_id））{
            $ this-> error（“页面$ wiki_id已被另一个用户锁定”）;
            出口（1）;
        }

        锁（$ wiki_id）;

        if（checklock（$ wiki_id））{
            $ this-> error（“无法获取$ wiki_id的锁”）;
            var_dump（checklock（$ wiki_id））;
            出口（1）;
        }
    }

    / **
     *清除给定页面上的锁
     *
     * @param字符串$ wiki_id
     * /
    受保护的函数clearLock（$ wiki_id）{
        if（$ this-> force）$ this-> deleteLock（$ wiki_id）;

        $ _SERVER ['REMOTE_USER'] = $ this->用户名；
        if（checklock（$ wiki_id））{
            $ this-> error（“页面$ wiki_id被另一个用户锁定”）;
            出口（1）;
        }

        解锁（$ wiki_id）;

        if（file_exists（wikiLockFN（$ wiki_id）））{
            $ this-> error（“无法清除$ wiki_id的锁定”）;
            出口（1）;
        }
    }

    / **
     *强制删除给定页面上的锁
     *
     * @param字符串$ wiki_id
     * /
    受保护的函数deleteLock（$ wiki_id）{
        $ wikiLockFN = wikiLockFN（$ wiki_id）;

        if（file_exists（$ wikiLockFN））{
            if（！unlink（$ wikiLockFN））{
                $ this-> error（“无法删除$ wikiLockFN”）;
                出口（1）;
            }
        }
    }

    / **
     *从环境中获取当前用户的用户名
     *
     * @返回字符串
     * /
    受保护的函数getUser（）{
        $ user = getenv（'USER'）;
        if（空（$ user））{
            $ user = getenv（'USERNAME'）;
        }其他{
            返回$ user；
        }
        if（空（$ user））{
            $ user ='管理员';
        }
        返回$ user；
    }
}

//主
$ cli =新的PageCLI（）;
$ cli-> run（）;