＃！/ usr / bin / php
<？php

使用splitbrain \ phpcli \ CLI;
使用splitbrain \ phpcli \ Options;

if（！defined（'DOKU_INC'））define（'DOKU_INC'，realpath（dirname（__ FILE__）。'/../'）。'/'）;
define（'NOSESSION'，1）;
require_once（DOKU_INC。'inc / init.php'）;

/ **
 *查找通缉页面
 * /
WantedPagesCLI类扩展了CLI {

    const DIR_CONTINUE = 1;
    const DIR_NS = 2;
    const DIR_PAGE = 3;

    私人$ skip = false;
    private $ sort ='想要的';

    私有的$ result = array（）;

    / **
     *在给定的$ options对象上注册选项和参数
     *
     * @param选项$ options
     * @返回无效
     * /
    受保护的功能设置（选项$ options）{
        $ options-> setHelp（
            “输出所需页面（尚不存在的页面）及其原始页面的列表”。
            （链接到这些缺失页面的页面）。
        ）;
        $ options-> registerArgument（
            “命名空间”，
            '要查找的名称空间。默认为根名称空间”，
            假
        ）;

        $ options-> registerOption（
            '分类'，
            “按需求或来源页面排序”，
            的，
            “（希望|来源）”
        ）;

        $ options-> registerOption（
            '跳跃'，
            “不显示第二维”，
            'k'
        ）;
    }

    / **
     *您的主程序
     *
     *参数和选项已在运行时解析
     *
     * @param选项$ options
     * @返回无效
     * /
    受保护的函数main（Options $ options）{
        $ args = $ options-> getArgs（）;
        if（$ args）{
            $ startdir = dirname（wikiFN（$ args [0]。'：xxx'））;
        }其他{
            $ startdir = dirname（wikiFN（'xxx'））;
        }

        $ this-> skip = $ options-> getOpt（'skip'）;
        $ this-> sort = $ options-> getOpt（'sort'）;

        $ this-> info（“ searching $ startdir”）;

        foreach（$ this-> get_pages（$ startdir）as $ page）{
            $ this-> internal_links（$ page）;
        }
        ksort（$ this-> result）;
        foreach（$ this-> result as $ main => $ subs）{
            if（$ this-> skip）{
                打印“ $ main \ n”；
            }其他{
                $ subs = array_unique（$ subs）;
                sort（$ subs）;
                foreach（$ subs as $ sub）{
                    printf（“％-40s％s \ n”，$ main，$ sub）;
                }
            }
        }
    }

    / **
     *确定搜索循环的方向
     *
     * @参数字符串$ entry
     * @参数字符串$ basepath
     * @return int
     * /
    受保护的函数dir_filter（$ entry，$ basepath）{
        if（$ entry =='。'|| $ entry =='..'）{
            返回WantedPagesCLI :: DIR_CONTINUE;
        }
        if（is_dir（$ basepath。'/'。$ entry））{
            if（strpos（$ entry，'_'）=== 0）{
                返回WantedPagesCLI :: DIR_CONTINUE;
            }
            返回WantedPagesCLI :: DIR_NS;
        }
        if（preg_match（'/ \。txt $ /'，$ entry））{
            返回WantedPagesCLI :: DIR_PAGE;
        }
        返回WantedPagesCLI :: DIR_CONTINUE;
    }

    / **
     *递归收集命名空间中的页面
     *
     * @参数字符串$ dir
     * @返回数组
     * @抛出DokuCLI_Exception
     * /
    受保护的函数get_pages（$ dir）{
        静态$ trunclen = null;
        if（！$ trunclen）{
            全球$ conf;
            $ trunclen = strlen（$ conf ['datadir']。'：'）;
        }

        if（！is_dir（$ dir））{
            抛出新的DokuCLI_Exception（“无法读取目录$ dir”）;
        }

        $ pages = array（）;
        $ dh = opendir（$ dir）;
        while（false！==（$ entry = readdir（$ dh）））{
            $ status = $ this-> dir_filter（$ entry，$ dir）;
            if（$ status == WantedPagesCLI :: DIR_CONTINUE）{
                继续;
            } else if（$ status == WantedPagesCLI :: DIR_NS）{
                $ pages = array_merge（$ pages，$ this-> get_pages（$ dir。'/'。$ entry））;
            }其他{
                $ page = array（
                    'id'=> pathID（substr（$ dir。'/'。$ entry，$ trunclen）），
                    '文件'=> $ dir。'/' $ entry，
                ）;
                $ pages [] = $ page;
            }
        }
        closeir（$ dh）;
        返回$ pages;
    }

    / **
     *解析指令并将不存在的链接添加到结果数组
     *
     * @param array $ page带有页面ID和文件路径的数组
     * /
    函数internal_links（$ page）{
        全球$ conf;
        $ instructions = p_get_instructions（file_get_contents（$ page ['file']））;
        $ cns = getNS（$ page ['id']）;
        $ exists = false；
        $ pid = $ page ['id'];
        foreach（$ instructions as $ ins）{
            if（$ ins [0] =='internallink'||（$ conf ['camelcase'] && $ ins [0] =='camelcaselink'））{
                $ mid = $ ins [1] [0];
                resolve_pageid（$ cns，$ mid，$ exists）;
                if（！$ exists）{
                    list（$ mid）=爆炸（'＃'，$ mid）; //记录没有哈希的页面

                    if（$ this-> sort =='origin'）{
                        $ this-> result [$ pid] [] = $ mid;
                    }其他{
                        $ this-> result [$ mid] [] = $ pid;
                    }
                }
            }
        }
    }
}

//主
$ cli =新的WantedPagesCLI（）;
$ cli-> run（）;