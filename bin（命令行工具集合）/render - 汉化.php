＃！/ usr / bin / php
<？php

使用splitbrain \ phpcli \ CLI;
使用splitbrain \ phpcli \ Options;

if（！defined（'DOKU_INC'））define（'DOKU_INC'，realpath（dirname（__ FILE__）。'/../'）。'/'）;
define（'NOSESSION'，1）;
require_once（DOKU_INC。'inc / init.php'）;

/ **
 *一个简单的命令行工具，以给定的格式呈现某些DokuWiki语法
 *渲染器。
 *
 *这可能不适用于期望特定环境的插件
 *在渲染之前进行设置，但应适用于大多数甚至所有标准
 * DokuWiki标记
 *
 * @许可证GPL2
 * @作者Andreas Gohr <andi@splitbrain.org>
 * /
类RenderCLI扩展了CLI {

    / **
     *在给定的$ options对象上注册选项和参数
     *
     * @param选项$ options
     * @返回无效
     * /
    受保护的功能设置（选项$ options）{
        $ options-> setHelp（
            “一个简单的命令行工具，用于使用给定的渲染器渲染某些DokuWiki语法。” 。
            “ \ n \ n”。
            “这可能不适用于期望特定环境的插件”。
            “在渲染之前进行设置，但应该适用于大多数甚至所有标准”。
            'DokuWiki标记'
        ）;
        $ options-> registerOption（'renderer'，'要使用的渲染器模式。默认为xhtml'，'r'，'mode'）;
    }

    / **
     *您的主程序
     *
     *参数和选项已在运行时解析
     *
     * @param选项$ options
     * @抛出DokuCLI_Exception
     * @返回无效
     * /
    受保护的函数main（Options $ options）{
        $ renderer = $ options-> getOpt（'renderer'，'xhtml'）;

        //采取行动
        $ source = stream_get_contents（STDIN）;
        $ info = array（）;
        $ result = p_render（$ renderer，p_get_instructions（$ source），$ info）;
        if（is_null（$ result））抛出新的DokuCLI_Exception（“没有这样的渲染器$ renderer”）;
        回声$结果;
    }
}

//主
$ cli =新的RenderCLI（）;
$ cli-> run（）;