＃！/ usr / bin / php
<？php

使用splitbrain \ phpcli \ CLI;
使用splitbrain \ phpcli \ Options;

if（！defined（'DOKU_INC'））define（'DOKU_INC'，realpath（dirname（__ FILE__）。'/../'）。'/'）;
define（'NOSESSION'，1）;
require_once（DOKU_INC。'inc / init.php'）;

/ **
 *从DokuWiki安装中删除不需要的语言
 * /
StripLangsCLI类扩展了CLI {

    / **
     *在给定的$ options对象上注册选项和参数
     *
     * @param选项$ options
     * @返回无效
     * /
    受保护的功能设置（选项$ options）{

        $ options-> setHelp（
            '除去指定语言之外的所有语言。英语 ' 。
            “永不删除！”
        ）;

        $ options-> registerOption（
            '保持'，
            “用逗号分隔的除英语外还保留的语言列表。”，
            'k'，
            “语言代码”
        ）;
        $ options-> registerOption（
            '仅限英语'，
            “删除除英语以外的所有语言”，
            'e'
        ）;
    }

    / **
     *您的主程序
     *
     *参数和选项已在运行时解析
     *
     * @param选项$ options
     * @返回无效
     * /
    受保护的函数main（Options $ options）{
        if（$ options-> getOpt（'keep'））{
            $ keep = explode（'，'，$ options-> getOpt（'keep'））;
            if（！in_array（'en'，$ keep））$ keep [] ='en';
        } elseif（$ options-> getOpt（'english-only'））{
            $ keep = array（'en'）;
        }其他{
            echo $ options-> help（）;
            退出（0）;
        }

        //杀死$ incs数组中除/ inc / lang和/ lib / plugins中的所有语言目录
        $ this-> stripDirLangs（realpath（dirname（__ FILE__）。'/../inc/lang'），$ keep）;
        $ this-> processExtensions（realpath（dirname（__ FILE__）。'/../lib/plugins'），$ keep）;
        $ this-> processExtensions（realpath（dirname（__ FILE__）。'/../lib/tpl'），$ keep）;
    }

    / **
     *从扩展中删除语言
     *
     * @param string $ path插件或模板目录的路径
     * @param array $ keep_langs语言保持
     * /
    受保护的函数processExtensions（$ path，$ keep_langs）{
        if（is_dir（$ path））{
            $ entries = scandir（$ path）;

            foreach（$ entries as $ entry）{
                if（$ entry！=“。” && $ entry！=“” ..“）{
                    if（is_dir（$ path。'/'。$ entry））{

                        $ plugin_langs = $ path。'/' $ entry。'/ lang';

                        if（is_dir（$ plugin_langs））{
                            $ this-> stripDirLangs（$ plugin_langs，$ keep_langs）;
                        }
                    }
                }
            }
        }
    }

    / **
     *从路径中剥离语言
     *
     * @param string $ lang dir的路径
     * @param array $ keep_langs语言保持
     * /
    受保护的功能stripDirLangs（$ path，$ keep_langs）{
        $ dir = dir（$ path）;

        while（（（$ cur_dir = $ dir-> read（））！== false）{
            if（$ cur_dir！='。'和$ cur_dir！='..'和is_dir（$ path。'/'。$ cur_dir））{

                if（！in_array（$ cur_dir，$ keep_langs，true））{
                    io_rmdir（$ path。'/'。$ cur_dir，true）;
                }
            }
        }
        $ dir-> close（）;
    }
}

$ cli =新的StripLangsCLI（）;
$ cli-> run（）;