<？php
/ **
 *此文件定义了多个可用许可，您可以许可
 *维基内容。不要更改此文件，而是创建一个
 * license.local.php代替。
 * /

$ license ['cc-zero'] = array（
    '名称'=>'CC0 1.0通用'，
    'url'=>'http://creativecommons.org/publicdomain/zero/1.0/'，
）;
$ license ['publicdomain'] = array（
    '名称'=>'公共域'，
    'url'=>'http://creativecommons.org/licenses/publicdomain/'，
）;
$ license ['cc-by'] = array（
    'name'=>'CC Attribution 4.0 International'，
    'url'=>'http://creativecommons.org/licenses/by/4.0/'，
）;
$ license ['cc-by-sa'] = array（
    'name'=>'CC Attribution-Share Alike 4.0 International'，
    'url'=>'http://creativecommons.org/licenses/by-sa/4.0/'，
）;
$ license ['gnufdl'] = array（
    '名称'=>'GNU自由文档许可证1.3'，
    'url'=>'http://www.gnu.org/licenses/fdl-1.3.html'，
）;
$ license ['cc-by-nc'] = array（
    'name'=>'CC Attribution-Noncommercial 4.0 International'，
    'url'=>'http://creativecommons.org/licenses/by-nc/4.0/'，
）;
$ license ['cc-by-nc-sa'] = array（
    'name'=>'CC Attribution-Noncommercial-Share Alike 4.0 International'，
    'url'=>'http://creativecommons.org/licenses/by-nc-sa/4.0/'，
）;
