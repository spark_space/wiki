<？php
/ **
 *这将配置哪些元数据可以通过以下方式进行编辑
 *媒体经理。数组的每个字段都是一个带有
 *以下内容：
 *字段名称-数据保存位置（EXIF或IPTC字段）
 * label-在$ lang变量中查找的键（如果未按原样打印）
 * htmltype-'text'，'textarea'或'date'
 *查找-数组附加字段以查找数据（EXIF或IPTC字段）
 *
 *字段不是连续排序的，因此无法插入其他项目
 *之间比较简单。
 *
 *这是一个PHP代码段，因此适用PHP语法。
 *
 *注意：$ fields不是全局变量，因此对任何字段都不可用
 *其他功能或模板稍后
 *
 *您可以在可选中扩展或覆盖此变量
 * conf / mediameta.local.php文件
 *
 *有关可用EXIF / IPTC字段的列表，请参阅
 * http://www.dokuwiki.org/devel:templates:detail.php
 * /


$ fields =数组（
    10 => array（'Iptc.Headline'，
                'img_title'，
                '文本'），

    20 =>数组（''，
                'img_date'，
                '日期'，
                array（'Date.EarliestTime'）），

    30 =>数组（''，
                'img_fname'，
                '文本'，
                array（'File.Name'）），

    40 => array（'Iptc.Caption'，
                'img_caption'，
                'textarea'，
                array（'Exif.UserComment'，
                      'Exif.TIFFImageDescription'，
                      'Exif.TIFFUserComment'）），

    50 =>数组（'Iptc.Byline'，
                'img_artist'，
                '文本'，
                array（'Exif.TIFFArtist'，
                      “ Exif.Artist”，
                      'Iptc.Credit'）），

    60 => array（'Iptc.CopyrightNotice'，
                'img_copyr'，
                '文本'，
                array（'Exif.TIFFCopyright'，
                      'Exif.Copyright'）），

    70 =>数组（''，
                'img_format'，
                '文本'，
                array（'File.Format'）），

    80 =>数组（''，
                'img_fsize'，
                '文本'，
                array（'File.NiceSize'）），

    90 =>数组（''，
                'img_width'，
                '文本'，
                array（'File.Width'）），

    100 =>数组（''，
                'img_height'，
                '文本'，
                array（'File.Height'）），

    110 =>数组（''，
                'img_camera'，
                '文本'，
                array（'Simple.Camera'）），

    120 => array（'Iptc.Keywords'，
                'img_keywords'，
                '文本'，
                array（'Exif.Category'）），
）;