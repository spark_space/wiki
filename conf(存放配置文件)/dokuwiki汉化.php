<？php
/ **
 *这是DokuWiki的主要配置文件
 *
 *所有默认值都保留在此处，您不应修改它，而应使用
 *一个local.php文件代替从此处覆盖设置。
 *
 *这是一段PHP代码，因此适用PHP语法！
 *
 *有关配置的帮助以及各种选项的详细说明
 *参见http://www.dokuwiki.org/config
 * /


/* 基本设置 */
$ conf ['title'] ='DokuWiki'; //标题中显示的内容
$ conf ['start'] ='开始'; //起始页名称
$ conf ['lang'] ='en'; //你的语言
$ conf ['template'] ='dokuwiki'; //请参阅lib / tpl目录
$ conf ['tagline'] =''; //标头中的标语（如果模板支持）
$ conf ['sidebar'] ='sidebar'; //根名称空间中侧边栏的名称（如果模板支持）
$ conf ['license'] ='cc-by-nc-sa'; //请参阅conf / license.php
$ conf ['savedir'] ='./data'; //存储所有文件的位置
$ conf ['basedir'] =''; // serveroot的绝对目录-自动检测为空白
$ conf ['baseurl'] =''; //包含协议的服务器URL-自动检测为空白
$ conf ['cookiedir'] =''; // Cookie中使用的路径-basedir为空白
$ conf ['dmode'] = 0755; //设置目录创建模式
$ conf ['fmode'] = 0644; //设置文件创建模式
$ conf ['allowdebug'] = 0; //允许调试输出，必要时启用0 | 1

/* 显示设置 */
$ conf ['recent'] = 20; //最近要显示多少个条目
$ conf ['recent_days'] = 7; //最近的更改要保留多少天。（天）
$ conf ['breadcrumbs'] = 10; //要显示多少个最近访问的页面
$ conf ['youarehere'] = 0; //显示“您在这里”导航？0 | 1
$ conf ['fullpath'] = 0; //仅显示文档的完整路径还是仅相对于datadir？0 | 1
$ conf ['typography'] = 1; // smartquote转换0 = off，1 =双引号，2 =所有引号
$ conf ['dformat'] ='％Y /％m /％d％H：％M'; // PHP接受的日期格式strftime（）函数
$ conf ['signature'] ='--- // [[@ MAIL @ | @ NAME @]] @DATE @ //'; //签名请参阅Wiki页面以获取详细信息
$ conf ['showuseras'] ='登录名'; //'loginname'用户登录名
                                          //'username'用户全名
                                          //'email'电子邮件地址（将根据mailguard进行混淆）
                                          //将'email_link'电子邮件地址作为mailto：链接（混淆）
$ conf ['toptoclevel'] = 1; //以开头和以下开头的级别包含在AutoTOC中（最多5个）
$ conf ['tocminheads'] = 3; //确定TOC是否已建立的最少标题
$ conf ['maxtoclevel'] = 3; //最多可以包含在AutoTOC中的级别（最多5个）
$ conf ['maxseclevel'] = 3; //在哪个级别上创建可编辑的部分（最多5个）
$ conf ['camelcase'] = 0; //使用CamelCase进行链接吗？（我不喜欢）0 | 1
$ conf ['deaccent'] = 1; //页面名称（1）或romanize（2）或keep（0）中的重音字符？
$ conf ['useheading'] = 0; //使用页面的第一个标题作为名称
$ conf ['sneaky_index'] = 0; //检查索引视图（0 | 1）中的名称空间读取权限（1可能导致意外行为）
$ conf ['hidepages'] =''; //正则表达式可从RSS，搜索和最新更改中跳过页面

/ *身份验证设置* /
$ conf ['useacl'] = 0; //使用访问控制列表来限制访问？
$ conf ['autopasswd'] = 1; //自动生成密码并将其通过电子邮件发送给用户
$ conf ['authtype'] ='authplain'; //应使用哪个身份验证后端
$ conf ['passcrypt'] ='smd5'; //使用的crypt方法（smd5，md5，sha1，ssha，crypt，mysql，my411）
$ conf ['defaultgroup'] ='用户'; //将默认的新用户组添加到
$ conf ['superuser'] ='!!未设置!!'; //管理员可以是user或@group或逗号分隔的列表user1，@ group1，user2
$ conf ['manager'] ='!!未设置!!'; //管理员可以是user或@group或逗号分隔的列表user1，@ group1，user2
$ conf ['profileconfirm'] = 1; //需要当前密码以确认对用户配置文件的更改
$ conf ['rememberme'] = 1; //启用/禁用登录时记住我
$ conf ['disableactions'] =''; //以逗号分隔的要禁用的动作列表
$ conf ['auth_security_timeout'] = 900; //时间（秒）的身份验证数据被视为有效，将其设置为0以在每次页面浏览时重新检查
$ conf ['securecookie'] = 1; //永远不要通过HTTP发送HTTPS cookie
$ conf ['remote'] = 0; //启用/禁用远程接口
$ conf ['remoteuser'] ='!!未设置!!'; //有权访问远程接口的用户/组（以逗号分隔）

/ *反垃圾邮件功能* /
$ conf ['usewordblock'] = 1; //根据文字阻止垃圾邮件？0 | 1
$ conf ['relnofollow'] = 1; //使用rel =“ nofollow”进行外部链接？
$ conf ['indexdelay'] = 60 * 60 * 24 * 5; //此时间（秒）后允许索引，默认值为5天
$ conf ['mailguard'] ='hex'; //针对垃圾邮件收集者混淆电子邮件地址？
                                         //有效的条目是：
                                         //'visible'-用[at]，替换@。带[点]和-带[破折号]
                                         //'hex'-使用十六进制实体对邮件地址进行编码
                                         //'none'-不要混淆地址
$ conf ['iexssprotect'] = 1; //检查上传文件中的JavaScript和HTML 0 | 1

/ *编辑设置* /
$ conf ['usedraft'] = 1; //编辑时自动保存草稿（0 | 1）
$ conf ['htmlok'] = 0; //可以嵌入原始HTML吗？这可能会破坏布局和XHTML的有效性0 | 1
$ conf ['phpok'] = 0; //是否可以嵌入PHP代码？永远不要在互联网上这样做！0 | 1
$ conf ['locktime'] = 15 * 60; //锁定文件的最长期限（默认为15分钟）
$ conf ['cachetime'] = 60 * 60 * 24; //缓存文件的最长期限（以秒为单位）（默认为一天）

/ *链接设置* /
//设置要在创建链接时使用的目标-在同一窗口中保留为空
$ conf ['target'] ['wiki'] ='';
$ conf ['target'] ['interwiki'] ='';
$ conf ['target'] ['extern'] ='';
$ conf ['target'] ['media'] ='';
$ conf ['target'] ['windows'] ='';

/ *媒体设置* /
$ conf ['mediarevisions'] = 1; //启用/禁用媒体修订
$ conf ['refcheck'] = 1; //删除媒体文件之前检查引用
$ conf ['gdlib'] = 2; // GDlib版本（0、1或2）2尝试自动检测
$ conf ['im_convert'] =''; // ImageMagicks转换的路径（将代替GD使用）
$ conf ['jpg_quality'] ='70'; //缩放jpg图像时的压缩质量（0-100）
$ conf ['fetchsize'] = 0; //最大大小（字节）fetch.php可以从extern下载，默认情况下处于禁用状态

/* 通知设置 */
$ conf ['subscribers'] = 0; //启用变更通知订阅支持
$ conf ['subscribe_time'] = 24 * 60 * 60; //摘要发送/列表发送的时间（以秒为单位，默认为1天）
                                         //应小于last_days中指定的时间
$ conf ['notify'] =''; //将更改信息发送到此电子邮件（任何人都留空）
$ conf ['registernotify'] =''; //将有关新注册用户的信息发送到此电子邮件（不留任何人为空白）
$ conf ['mailfrom'] =''; //发送邮件时使用此电子邮件
$ conf ['mailreturnpath'] =''; //将此电子邮件用作退回邮件的返回路径
$ conf ['mailprefix'] =''; //使用它作为外发邮件的前缀
$ conf ['htmlmail'] = 1; //发送HTML多部分邮件

/ *联合发布设置* /
$ conf ['sitemap'] = 0; //创建Google网站地图？多常？在几天内。
$ conf ['rss_type'] ='rss1'; //默认提供的RSS feed的类型：
                                         //'rss'-RSS 0.91
                                         //'rss1'-RSS 1.0
                                         //'rss2'-RSS 2.0
                                         //'atom'-Atom 0.3
                                         //'atom1'-Atom 1.0
$ conf ['rss_linkto'] ='diff'; //什么页面RSS条目链接到：
                                         //'diff'-显示修订差异的页面
                                         //'page'-修改后的页面本身
                                         //'rev'-显示所有修订的页面
                                         //'当前'-页面的最新修订
$ conf ['rss_content'] ='抽象'; //默认情况下要放入什么？
                                         //'摘要'-纯文本，第一段左右
                                         //'diff'-用<pre>标签包装的纯文本统一diff
                                         //'htmldiff'-diff作为HTML表
                                         //'html'-用XHTML呈现的完整页面
$ conf ['rss_media'] ='两者'; //应该列出什么？
                                         //“两者”-页面和媒体更改
                                         //'页面'-仅页面更改
                                         //'media'-仅媒体更改
$ conf ['rss_update'] = 5 * 60; //每n秒更新一次RSS提要（默认为5分钟）
$ conf ['rss_show_summary'] = 1; //在标题中添加修订摘要？0 | 1

/* 高级设置 */
$ conf ['updatecheck'] = 1; //自动检查是否有新版本？
$ conf ['userewrite'] = 0; //这样可以生成不错的网址：0：关闭1：.htaccess 2：内部
$ conf ['useslash'] = 0; //使用斜杠代替冒号？仅在重写处于打开状态时
$ conf ['sepchar'] ='_'; //页面名称中的单词分隔符；可能是
                                         //字母，数字，“ _”，“-”或“。”。
$ conf ['canonical'] = 0; //所有URL是否应使用完整的规范http：// ...样式？
$ conf ['fnencode'] ='url'; //编码文件名（url | safe | utf-8）
$ conf ['autoplural'] = 0; //尝试不存在的文件的（非）复数形式？
$ conf ['compression'] ='gz'; //压缩旧版本：（0：关闭）（'gz'：gnuzip）（'bz2'：bzip）
                                         // bz2生成较小的文件，但需要更多的cpu功能
$ conf ['gzip_output'] = 0; //对输出的xhtml使用gzip内容编码（如果浏览器允许）
$ conf ['compress'] = 1; //从样式和JavaScript中剥离空格和注释？1 | 0
$ conf ['cssdatauri'] = 512; //要嵌入CSS的小图片的最大字节大小，不适用于IE <8
$ conf ['send404'] = 0; //为不存在的页面发送HTTP 404状态？
$ conf ['broken_iua'] = 0; //带有损坏的ignore_user_abort（IIS + CGI）的平台0 | 1
$ conf ['xsendfile'] = 0; //使用X-Sendfile（1 = lighttpd，2 = standard）
$ conf ['renderer_xhtml'] ='xhtml'; //用于主页面生成的渲染器
$ conf ['readdircache'] = 0; //用于readdir操作的秒缓存时间，0禁用。
$ conf ['search_nslimit'] = 0; //将搜索限制为当前的X名称空间
$ conf ['search_fragment'] ='精确'; //指定默认的片段搜索行为

/* 网络设置 */
$ conf ['dnslookups'] = 1; //禁用以禁止IP到主机名查找
$ conf ['jquerycdn'] = 0; //使用CDN传递jQuery吗？
//代理设置-如果您的服务器需要代理来访问网络，请设置这些
$ conf ['proxy'] ['host'] ='';
$ conf ['proxy'] ['port'] ='';
$ conf ['proxy'] ['user'] ='';
$ conf ['proxy'] ['pass'] ='';
$ conf ['proxy'] ['ssl'] = 0;
$ conf ['proxy'] ['except'] ='';
// Safemode Hack-阅读http://www.dokuwiki.org/config:safemodehack！
$ conf ['safemodehack'] = 0;
$ conf ['ftp'] ['host'] ='本地主机';
$ conf ['ftp'] ['port'] ='21';
$ conf ['ftp'] ['user'] ='用户';
$ conf ['ftp'] ['pass'] ='密码';
$ conf ['ftp'] ['root'] ='/ home / user / htdocs';

