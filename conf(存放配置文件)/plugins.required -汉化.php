<？php
/ **
 *此文件配置插件的启用/禁用状态，这些插件也受到保护
 *来自扩展管理器的更改。这些设置将覆盖所有本地设置。
 *不建议更改此文件，因为它在DokuWiki升级中被覆盖。
 * /
$ plugins ['acl'] = 1;
$ plugins ['authplain'] = 1;
$ plugins ['extension'] = 1;
$ plugins ['config'] = 1;
$ plugins ['usermanager'] = 1;
$ plugins ['template：dokuwiki'] = 1; //不是插件，但也不应卸载